---
title: "Page d'accueil"
order: 0
in_menu: true
---
Cours de Soutien
===========
Vous trouverez ici des feuilles **d'exercices**, des **résumer de cours** et des **QCM**.

Je suis étudiant en mathématiques et je souhaites devenir enseignant. C'est donc avec plaisir que je met mes connaissance à contribution afin de vous aidez pour les notions que vous avez du mal à comprendre. 